from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import re

def validate_phonenumber(phonenumbervalue):
        print(f'the phonenumber value passed to validator is ', phonenumbervalue)
        pattern = re.compile('(0|91){1}?[6-9][0-9]{9}')
        if not pattern.match(phonenumbervalue):
            raise ValidationError(
                _('phonenumber is not a valid phone number. the phone number should be in the following format 91xxxxxxxxxx'), params={'phonenumber':phonenumbervalue}
            )