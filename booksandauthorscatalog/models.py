from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import re
from .validators import *

#model class for Author table
class Author(models.Model):
    name = models.CharField(max_length = 100, unique = True, null = False)
    phonenumber = models.CharField(max_length = 12, blank = False, unique = True, validators = [validate_phonenumber])
    birthdate = models.DateField(null = False)
    deathdate = models.DateField(blank = True, default = None, null = True)


#model class for category table
class Category(models.Model):
    name = models.CharField(max_length = 100, unique = True, blank = False)
    

#model class for book table
class Book(models.Model):
    title = models.CharField(max_length = 100, unique = True, null = False)
    author_id = models.ForeignKey(Author, null =True, on_delete = models.SET_NULL, blank = False)
    category_id = models.ForeignKey(Category, null = True, blank = False, on_delete = models.SET_NULL)
    publisher = models.CharField(max_length = 100, blank = False)
    published_date = models.DateField(blank = False)
    price = models.FloatField(blank = False)
    sold_count = models.IntegerField(blank = True)


