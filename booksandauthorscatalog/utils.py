from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from .models import Author, Book, Category
from .serializers import AuthorSerializer, BookSerializer, CategorySerializer
import io


#A Method that takes a string and return JSON response
def jsonResponse(msg):
    res = {'res':msg}
    json_response = JSONRenderer().render(res)
    return json_response
   