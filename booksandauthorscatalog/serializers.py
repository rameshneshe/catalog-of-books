from rest_framework import serializers
from .models import Author, Book, Category
from rest_framework.validators import ValidationError
import re

# serializer for author class
class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'
    

# serializer for Book class
class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'

# serializer for category class
class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'