from django.contrib import admin
from .models import Author, Book, Category


#Author model registration
@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ['id','name','phonenumber','birthdate','deathdate']

#Book model registration
@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ['id','title','publisher','price','sold_count']

#Category model registration
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id','name']