from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from .models import Author, Book, Category
from .serializers import AuthorSerializer, BookSerializer, CategorySerializer
import io
from django.db.models import Max
from .utils import *
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt

#Author: PANYAM RAMESH

#Author View
class Author(View):
    # This method will give you the list of all author names
    def get(self, request):
        try:
            from .models import Author
            # Fething all the author names and storing it in a list
            authors = [author.name for author in Author.objects.all()]
            print(f'authors frmo the models are ', authors)
            return HttpResponse(jsonResponse(authors), content_type = 'application/json') 
        except Exception as e:
            return HttpResponse(jsonResponse(e), content_type = 'application/json')

   # This method will add the new author to DB
    @csrf_exempt
    def post(self, request):
        try:
            formdata = request.POST
            serializer = AuthorSerializer(data = formdata)
            # Validating and storing the author object on DB
            if serializer.is_valid():
                serializer.save()
                return HttpResponse(jsonResponse('Author Added Successfully.'), content_type = 'application/json')
            return HttpResponse(jsonResponse(serializer.errors), content_type = 'application/json')
        except Exception as e:
            return HttpResponse(jsonResponse(e), content_type = 'application/json')
#Book View
class Book(View):
    # This method will give you the list of all books written by particular author 
    # if we pass an authod id if not it will give you list of all books
    def get(self, request):
        try:
            from .models import Book
            params = request.GET
            author_id = params.get('id')
            books_json = []
            if author_id:
                # Fetching and storing the list of authors based on authors id  and storing it in the json list
                books = Book.objects.filter(author_id = author_id)
                books_json = [{"title":book.title,"author":book.author_id.name, "price":book.price, "publisher":book.publisher, "sold_count":book.sold_count}for book in books]
                return HttpResponse(jsonResponse(books_json), content_type = 'application/json')
            books = Book.objects.all()
            books_json = [{"title":book.title,"author":book.author_id.name, "price":book.price, "publisher":book.publisher, "sold_count":book.sold_count}for book in books]
            return HttpResponse(jsonResponse(books_json), content_type = 'application/json')
        except Exception as e:
            return HttpResponse(jsonResponse(e), content_type = 'application/json')

    # This method will add new book to the DB
    @csrf_exempt
    def post(self, request):
        try:
            formdata = request.POST
            serializer = BookSerializer(data = formdata)
            # Validating the form data and storing it on DB
            if serializer.is_valid():
                serializer.save()
                return HttpResponse(jsonResponse('Book Added Successfully.'), content_type = 'application/json')
            return HttpResponse(jsonResponse(serializer.errors), content_type = 'application/json')
        except Exception as e:
            return HttpResponse(jsonResponse(str(e)), content_type = 'application/json')
    
    
class Category(View):
    # This method will give list of all categories
    def get(self, request):
        try:
            from .models import Category
            categories = Category.objects.all()
            category_json = [{"category_id":category.id, "categoryname": category.name}for category in categories]
            return HttpResponse(jsonResponse(category_json), content_type = 'application/json')               
        except Exception as e:
            return HttpResponse(jsonResponse(str(e)), content_type = 'application/json')

    # This method will add new category to DB
    @csrf_exempt
    def post(self, request):
        try:
            formdata = request.POST
            serializer = CategorySerializer(data = formdata)
            # validating the form data
            if serializer.is_valid():
                serializer.save()
                return HttpResponse(jsonResponse("Category Added Successfully."), content_type = 'application/json')
            return HttpResponse(jsonResponse(serializer.errors), content_type = 'application/json')
        except Exception as e:
            return HttpResponse(jsonResponse(str(e)), content_type = 'application/json')

# Search View
class Search(View):
    # This method will give list of books by partial book_title or by partial author_name or by both
    def get(self, request):
        #import pdb;pdb.set_trace()
        from .models import Book, Author
        try:
            parameter = request.GET
            title = parameter.get('title')
            name = parameter.get('name')
            json_dict = {}
            author_json_dict=[]
            auth_json_dict=[]
            title_json_dict=[]
            if title:
                # Fetching the books list whose title contains text {title}
                books = Book.objects.filter(title__icontains=title)
                # Storing the books list in json list object
                title_json_dict= [{"name":book.title,"id":book.pk,"category":book.category_id.name} for book in books]
                    
            if name:
                # Fetching the list of author id's whose name contains text {name}
                authors = Author.objects.filter(name__icontains = name).values('pk')
                for author in authors:
                    # Fetching the list of books belongs to (authod_id) author
                    books= Book.objects.filter(author_id = author['pk'])
                    # Storing the list of books in json list
                    author_book_json_dict= [{"name":book.title,"id":book.pk,"category":book.category_id.name, "Author_name":book.author_id.name} for book in books]
                    # Appending authors book in another json list to avoid data loss
                    auth_json_dict.append(author_book_json_dict)   
            if title_json_dict:
                json_dict.update({"Title Search":title_json_dict})
            if auth_json_dict:
                json_dict.update({"Author Search":auth_json_dict})
            return HttpResponse(jsonResponse(json_dict), content_type = 'application/json')
                
        except Exception as e:
            return HttpResponse(jsonResponse(e), content_type = 'application/json')
# MostSold View
class MostSold(View):
    # This method will give list of most sold books by category_id or by authod_id
    def get(self, request):
        try: 
            from .models import Book
            params = request.GET
            category_id = params.get('category_id')
            author_id = params.get('author_id')
            if author_id:
                books_json=[]
                # Fetching the maximum sales count out of all books belongs to particular author
                max_sales_count = Book.objects.filter(author_id = author_id).aggregate(Max('sold_count')).get('sold_count__max')
                # Fetching the list of books by authors whose sales count is maximul of all books
                books = Book.objects.filter(author_id = author_id).filter(sold_count=max_sales_count)
                books_json = [{"title":book.title, "author_name": book.author_id.name, "price": book.price, "sold_count":book.sold_count} for book in books]
                return HttpResponse(jsonResponse(books_json), content_type = 'application/json')
            if category_id:
                books_json = []
                # Fetching the maximum sales count out of all books belongs to particular category
                max_sales_count = Book.objects.filter(category_id = category_id).aggregate(Max('sold_count')).get('sold_count__max')
                # Fetching the list of books by categories whose sales count is maximul of all books
                books = Book.objects.filter(category_id = category_id).filter(sold_count=max_sales_count)
                books_json = [{"title":book.title, "author_name": book.author_id.name, "price": book.price, "sold_count":book.sold_count} for book in books]
                return HttpResponse(jsonResponse(books_json), content_type = 'application/json')
        except Exception as e:
            return HttpResponse(jsonResponse(e), content_type = 'application/json')

