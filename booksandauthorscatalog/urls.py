from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from .views import *
from django.views.decorators.csrf import csrf_exempt

# URL declarations
urlpatterns = [
    url(r'^author/$', csrf_exempt(Author.as_view())),
    url(r'^book/$', csrf_exempt(Book.as_view())),
    url(r'^category/$', csrf_exempt(Category.as_view())),
    url(r'^search/$', csrf_exempt(Search.as_view())),
    url(r'^mostsold/$', csrf_exempt(MostSold.as_view())),
]