from django.apps import AppConfig


class BooksandauthorscatalogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'booksandauthorscatalog'
