
# E-Book Store

An online Book store where you can post a new book and can fetch list of existing books and we can search books by author and by category 
and with some other features like fetching most sold books and searching book by partial title and by partial author name


## API Reference
### base_url = http://localhost:8000/book_catalog/


### Author View:

#### Get list of all authors
```http
  GET /author/
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `` | `` | just by the api url you will get the list of all authors |

#### Post Author data

```http
  POST /author/
```

| Form data Parameter's | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `name`      | `string` | **Required**. name of author  |
| `phonenumber`|  `string`|**Required**. phone number of author |
| `birthdate` |`date`|**Required**.author's date of birth |
| `deathdate` |`date`|**Optional**.author's date of death|

Takes atlest 3 arguments and will add new author to data base



### Book View:

#### Get list of all books based on author_id
```http
  GET /book/
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `id` | `string` | **Required**. id of the author |

#### Post Book data

```http
  POST /book/
```

| Form data Parameter's | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `title`      | `string` | **Required**. title of the book  |
| `author_id`|  `string`|**Required** id of the author |
| `publisher` |`string`|**Required** book's publisher name |
| `category_id` |`string`|**Optional** id of the category|
| `published_date`|  `date`|**Required** book published date |
| `price` |`decimal number`|**Required** price of the book |
| `sold_count` |`number`|**Optional** count of the books sold|

Takes 7 arguments and will add new book to data base


### Category View:

#### Get list of all categories
```http
  GET /category/
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `` | `` |  no params required |

#### Post Book data

```http
  POST /category/
```

| Form data Parameter's | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `name`      | `string` | **Required**. name of the category  |

Takes 1 argument and will add new category to data base


### Search View:

#### Get list of all books which contains the search text
```http
  GET /search/
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `title` | `string` | **Required**. title of the books |
| `name` | `string` | **Required**. name of the author |


Takes atleast 1 argument and will give you the list of books whose title contains the text of {param:title} or the book written by author {param: name}

### MostSold View:

#### Get list of all books which most sold based on author_id or by category_id
```http
  GET /search/
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `author_id` | `string` | **Required**. title of the books |
| `category_id` | `string` | **Required**. name of the author |


Takes atleast 1 argument and will give you the list of most sold books by the author {param: author_id} or by the category {param: category_id} 

## Author

- [@panyamramesh](https://bitbucket.org/rameshneshe/)

PANYAM RAMESH